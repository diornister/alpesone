<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Goutte;

class CrawlerController extends Controller
{
    public $array = array();
    private $url = '';

    /**
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->url = 'https://www.seminovosbh.com.br/destaques/particulares';

        $this->crawler($this->url, 'body > div > dl > dt > a', 'index');

        return response()->json([
            'message' => $this->array
        ]);
    }

    /**
     * @param  [string] url
     * @param  [string] filter
     * @param  [string] each
     * @return \Illuminate\Http\Response
     */
    private function crawler($url, $filter, $each){
        $crawler = Goutte::request('GET', $url);
        $crawler->filter($filter)->each(function ($node, $i) use ($each) {            
            if($each == 'index'){
                array_push($this->array, $i . ' = ' . $node->attr('href'));                
            }else{
                array_push($this->array, $i . ' = ' . $node->text());
            }
        });
    }
    
    private function urlBuilder(Request $request){
        if($request->veicle){
            $this->url .= '/'.$request->veicle;
        }

        if($request->conservation){
            $this->url .= '/estado-conservacao/'.$request->conservation;
        }

        if($request->value1){
            $this->url .= '/valor1/'.$request->value1;
        }

        if($request->value2){
            $this->url .= '/valor2/'.$request->value2;
        }

        if($request->year1){
            $this->url .= '/ano1/'.$request->year1;
        }

        if($request->year2){
            $this->url .= '/ano2/'.$request->year2;
        }

        $request->user
            ? $this->url .= '/usuario/'.$request->user
            : $this->url .= '/usuario/todos';
    }

    /**
     * 
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $mainURL = 'https://www.seminovosbh.com.br/resultadobusca/index/veiculo';

        $this->urlBuilder($request);

        $this->crawler($mainURL.$this->url, 'div#topoConteudoBusca > dl.bg-busca ', 'search');        

        foreach ($this->array as $key => $value) {
            $this->array[$key] = str_replace("\n", '', $value);
        }

        return response()->json([
            'message' => $this->array
        ]);        
    }    
}
